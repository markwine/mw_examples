import arrow
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output

from .datamodels import OrderRequest

TODAY = arrow.now().to("US/Pacific")
DEFAULT_START = TODAY.shift(weekday=6).shift(weeks=-1)
DEFAULT_END = TODAY.shift(weekday=5).shift(weeks=-1) if TODAY.weekday() == 6 else TODAY

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

order_request = OrderRequest()


app.layout = html.Div(
    children=[
        html.H1(children="Clover Dashboard"),
        dcc.DatePickerRange(
            id="sales_range",
            start_date=DEFAULT_START.date(),
            end_date=DEFAULT_END.date(),
        ),
        html.Div(
            [
                html.Div(
                    [dcc.Graph(id="ut_figure", figure=order_request.ut_figure)],
                    className="four columns",
                ),
                html.Div(
                    [dcc.Graph(id="at_figure", figure=order_request.at_figure)],
                    className="four columns",
                ),
                html.Div(
                    [
                        dcc.Graph(
                            id="sales_figure",
                            figure={
                                "data": [
                                    {
                                        "x": [0],
                                        "y": [order_request.get_sales_totals()],
                                        "type": "bar",
                                        "name": "this week",
                                    },
                                    {
                                        "x": [0],
                                        "y": [order_request.get_sales_totals()],
                                        "type": "bar",
                                        "name": "last week",
                                    },
                                ],
                                "layout": {"title": "Sales Totals"},
                            },
                        )
                    ],
                    className="four columns",
                ),
            ],
            className="row",
        ),
        dcc.RadioItems(
            id="days-in",
            options=[
                {"label": "All", "value": [0, 1, 2, 3, 4, 5, 6]},
                {"label": "Wednesday", "value": [2]},
                {"label": "Thursday", "value": [3]},
                {"label": "Friday", "value": [4]},
                {"label": "Saturday", "value": [5]},
                {"label": "Sunday", "value": [6]},
                {"label": "All Weekdays", "value": [0, 1, 2, 3, 4]},
                {"label": "Weekend", "value": [5, 6]},
            ],
            value=[0, 1, 2, 3, 4, 5, 6],
            labelStyle={"display": "inline-block"},
        ),
        html.Div(id="days-div"),
        dcc.Graph(
            id="hourly_histogram",
            figure={
                "data": [
                    {
                        "x": list(range(24))[8:21],
                        "y": order_request.get_hourly_revenue()[8:21],
                        "type": "bar",
                        "name": "hour",
                    }
                ]
            },
        ),
        dcc.Input(id="n_items", type="number", value=15),
        dash_table.DataTable(
            id="item_table",
            columns=[
                {"name": "name", "id": "name"},
                {"name": "revenue", "id": "revenue"},
                {"name": "number", "id": "number"},
            ],
            style_cell={"textAlign": "left"},
            sorting="be",
            sorting_type="single",
            sorting_settings=[{"column_id": "revenue", "direction": "desc"}],
        ),
        dcc.Graph(id="pareto_items_figure", figure=order_request.pareto_items_figure()),
    ]
)


@app.callback(
    Output("ut_figure", "figure"),
    [Input("sales_range", "start_date"), Input("sales_range", "end_date")],
)
def update_ut(start_date, end_date):
    """
    Update units per transaction figure based on change in relevant inputs


    :param start_date: Start date for orders
    :param end_date: End date for orders
    :return: Figure
    """
    order_request.update_calendar(arrow.get(start_date), arrow.get(end_date))
    return order_request.ut_figure


@app.callback(
    Output("at_figure", "figure"),
    [Input("sales_range", "start_date"), Input("sales_range", "end_date")],
)
def update_at(start_date, end_date):
    """
    Update average transaction figure based on change in relevant inputs


    :param start_date: Start date for orders
    :param end_date: End date for orders
    :return: Figure
    """
    order_request.update_calendar(arrow.get(start_date), arrow.get(end_date))
    return order_request.at_figure


@app.callback(
    Output("sales_figure", "figure"),
    [Input("sales_range", "start_date"), Input("sales_range", "end_date")],
)
def update_sales(start_date, end_date):
    """
    Update sales data figure based on change in relevant inputs

    :param start_date: Start date for orders
    :param end_date: End date for orders
    :return: figure
    """
    order_request.update_calendar(arrow.get(start_date), arrow.get(end_date))
    return order_request.sales_figure


@app.callback(
    Output("pareto_items_figure", "figure"),
    [
        Input("sales_range", "start_date"),
        Input("sales_range", "end_date"),
        Input("n_items", "value"),
    ],
)
def update_pareto_figure(start_date, end_date, n_items):
    """
    Update pareto figure of item sales based on change in relevant input

    :param start_date: Start date for orders
    :param end_date: End date for orders
    :param n_items: Number of items to display on pareto figure
    :return: Figure
    """
    order_request.update_calendar(arrow.get(start_date), arrow.get(end_date))
    return order_request.pareto_items_figure(n_items=n_items)


@app.callback(
    Output("hourly_histogram", "figure"),
    [
        Input("sales_range", "start_date"),
        Input("sales_range", "end_date"),
        Input("days-in", "value"),
    ],
)
def update_hourly_revenue(start_date, end_date, days):
    """
    Update Histogram of sales data based on change in relevant input

    :param start_date: Start date for orders
    :param end_date: End date for orders
    :param days: list of days coming from dash input
    :return: Figure
    """
    order_request.update_calendar(
        start_date=arrow.get(start_date), end_date=arrow.get(end_date)
    )
    return order_request.hourly_revenue(days)


@app.callback(
    Output("item_table", "data"),
    [
        Input("sales_range", "start_date"),
        Input("sales_range", "end_date"),
        Input("item_table", "sorting_settings"),
        Input("n_items", "value"),
    ],
)
def update_item_list(start_date, end_date, sorting_settings, n_items):
    """
    Update item table on change of relevant input

    :param start_date: Start date for orders
    :param end_date: End date for orders
    :param sorting_settings: List of settings coming from dash
    :param n_items: Number of items to display
    :return: Table
    """
    order_request.update_calendar(arrow.get(start_date), arrow.get(end_date))
    items = order_request.get_item_sales(
        start_date=arrow.get(start_date), end_date=arrow.get(end_date)
    ).items()
    if sorting_settings:
        sorted_items = list(
            sorted(
                items,
                key=lambda x: float(
                    x[1].get(sorting_settings[0].get("column_id", "revenue"))
                ),
                reverse=(sorting_settings[0].get("direction") == "desc"),
            )
        )
    else:
        sorted_items = list(items)
    item_table = [
        {
            "name": item[0],
            "revenue": round(item[1].get("revenue", 0), 2),
            "number": item[1].get("number"),
        }
        for item in sorted_items[:n_items]
    ]
    return item_table


if __name__ == "__main__":
    app.run_server(debug=True)
