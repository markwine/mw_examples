from collections import Counter, defaultdict
from datetime import datetime, timedelta
import os
import time

import arrow
import pytz
import requests

CLOVER_API_URL = "https://api.clover.com/v3/"
MERCHANT_ID = os.environ.get("CLOVER_MERCHANT_ID")
APB_TOKEN = os.environ.get("CLOVER_APB_TOKEN")
EXCLUDED_ITEMS = frozenset(
    os.environ.get("CLOVER_EXCLUDED_ITEMS") or {}
)  # Line Items to ignore

TODAY = arrow.now().to("US/Pacific")
DEFAULT_END = TODAY.shift(weekday=5).shift(weeks=-1) if TODAY.weekday() == 6 else TODAY
DEFAULT_START = TODAY.shift(weekday=6).shift(weeks=-1)


class OrderRequest:
    """
    Class for turning order requests from Clover API into displayable elements

    :param merchant_id: Clover Merchant id
    :param excluded_items: Frozenset of skus to exclude from calculations, e.g. shopping bags
    """

    def __init__(self, merchant_id=MERCHANT_ID, excluded_items=EXCLUDED_ITEMS):
        self.merchant_id = merchant_id
        self.excluded_items = excluded_items
        self.end_date = arrow.now().to("US/Pacific")
        self.start_date = self.end_date.shift(weekday=6).shift(weeks=-1)
        self.prior_end = self.start_date.shift(days=-1)
        self.prior_start = self.start_date.shift(weeks=-1)
        self.month_start = self.end_date.shift(months=-1)
        self.order_data = None

    @staticmethod
    def _get_order_data(
        start_time=datetime.now() - timedelta(31), end_time=datetime.now(), limit=1000
    ):
        """
        Makes request from Clover API and return list of data for each order

        :param Start_time (arrow.Arrow): starting time of data request Need to multiply by 1000 for Clover API
        :param End_time (arrow.Arrow): ending time of data request
        :param limit (int): Max number of orders to request at once - will make multiple requests if needed
        :return (list): Order data from clover over time period
        """
        date_filters = (
            f"&filter=clientCreatedTime>={int(start_time.timestamp()*1000)}"
            f"&filter=clientCreatedTime<={int(end_time.timestamp()*1000)}"
        )
        order_url = f"{CLOVER_API_URL}merchants/{MERCHANT_ID}/orders?limit={limit}{date_filters}&expand=lineItems.items"
        auth_header = {"Authorization": f"Bearer {APB_TOKEN}"}
        order_request = requests.get(order_url, headers=auth_header)
        if order_request.status_code == 404:
            return "Data Unavailable"  # raise error
        order_json = order_request.json()
        order_list = order_json.get("elements", [])
        offset = 1
        sleep_time = 1.01  # Clover's request reset time
        while len(order_json.get("elements", [])) == limit:
            order_request = requests.get(
                order_url, headers=auth_header, params={"offset": offset * limit}
            )
            if order_request.status_code == 404:
                pass
            elif order_request.status_code == 429:
                if sleep_time > 5:
                    pass
                time.sleep(sleep_time)
                sleep_time += 1
                continue
            order_json = order_request.json()
            order_list += order_json.get("elements")
            offset += 1
        return order_list or [{"total": 0, "line_items": {"elements": []}}]

    def eligible_orders(self, start_date=None, end_date=None):
        """
        Construct list of orders that fit request criteria

        :param start_date: Start date for specific set of orders
        :param end_date: End date for specific set of orders
        :return (list): List of eligible orders
        """
        if not self.order_data:
            self.order_data = self._get_order_data()
        if start_date is None:
            start_date = self.start_date
        if end_date is None:
            end_date = self.end_date
        eligible_orders = [
            order
            for order in self.order_data
            if start_date.floor("day").timestamp * 1000
            < order.get("createdTime")
            < end_date.ceil("day").timestamp * 1000
            and order.get("state") != "open"
        ]
        return eligible_orders

    def get_units_per_transaction(self, start_date=None, end_date=None):
        """
        Calculate summary - units per transaction, the average number of non-excluded line items in the eligible orders

        :param start_date: Start date for summary statistic
        :param end_date: End date for summary statistic
        :return (float): Units per transaction
        """
        if not self.order_data:
            self.order_data = self._get_order_data()
        starting_date = start_date or self.start_date
        ending_date = end_date or self.end_date
        eligible_orders = self.eligible_orders(
            start_date=starting_date, end_date=ending_date
        )
        real_items = [
            item
            for order in eligible_orders
            if order.get("lineItems")
            for item in order["lineItems"]["elements"]
            if item.get("item", {"id": None})["id"] not in EXCLUDED_ITEMS
        ]
        if len(eligible_orders):
            units_per_transaction = len(real_items) / len(eligible_orders)
        else:
            units_per_transaction = 0
        return units_per_transaction

    def get_average_transaction(self, start_date=None, end_date=None):
        """
        Calculate summary - average transaction, the average revenue in the eligible orders

        :param start_date: Start date for summary statistic
        :param end_date: End date for summary statistic
        :return (float): Average transaction
        """
        if not self.order_data:
            self.order_data = self._get_order_data()
        starting_date = start_date
        ending_date = end_date
        eligible_orders = self.eligible_orders(
            start_date=starting_date, end_date=ending_date
        )
        if len(eligible_orders):
            average_transaction = (
                sum([order.get("total") for order in eligible_orders])
                / len(eligible_orders)
                / 100
            )
        else:
            average_transaction = 0
        return average_transaction

    def get_sales_totals(self, start_date=None, end_date=None):
        """
        Calculate summary - sum of sales for eligible orders

        :param start_date: Start date for summary statistic
        :param end_date: End date for summary statistic
        :return (float):
        """
        if not self.order_data:
            self.order_data = self._get_order_data()
        starting_date = start_date or self.start_date
        ending_date = end_date or self.end_date
        eligible_orders = self.eligible_orders(
            start_date=starting_date, end_date=ending_date
        )
        total_sales = sum([order.get("total") / 100 for order in eligible_orders])
        return total_sales

    def get_item_sales(self, start_date=None, end_date=None):
        """
        Calculate summary by item - total sales for every item in eligible orders

        :param start_date: Start date for summary statistic
        :param end_date: End date for summary statistic
        :return (dict): Dictionary of item sales
        """
        if not self.order_data:
            self.order_data = self._get_order_data()
        starting_date = start_date or self.start_date  # start_date
        ending_date = end_date or self.end_date
        eligible_orders = self.eligible_orders(
            start_date=starting_date, end_date=ending_date
        )
        item_sales = defaultdict(Counter)
        skus = set()
        for order in eligible_orders:
            if not order.get("lineItems"):
                continue
            for item in order["lineItems"]["elements"]:
                if item.get("item", {"id": None})["id"] not in EXCLUDED_ITEMS:
                    item_sales[item.get("name")].update(
                        revenue=round(item.get("price", 0) / 100, 2), number=1
                    )
                    try:
                        skus.add(
                            (
                                int(item.get("item", {"sku": ""}).get("sku", "")),
                                item.get("name"),
                            )
                        )
                    except ValueError:
                        continue
        return item_sales

    def pareto_items_figure(self, n_items=15):
        """
        Construct data for pareto diagram of item sales. Pareto diagram is ordered bar chart.

        :param n_items: Number of items to display on diagram
        :return (dict): Data dictionary to be used by dash figure constructor
        """
        items = self.get_item_sales(
            start_date=arrow.get(self.start_date), end_date=arrow.get(self.end_date)
        ).items()
        sorted_items = list(
            sorted(items, key=lambda x: float(x[1].get("revenue")), reverse=True)
        )[:n_items]
        data = {
            "x": [item[0] for item in sorted_items],
            "y": [item[1].get("revenue") for item in sorted_items],
            "type": "bar",
            "name": "Sales Distribution",
        }
        figure = {"data": [data]}
        return figure

    def get_hourly_revenue(self, days=None, start_date=None, end_date=None):
        """
        Construct dictionary of hours and revenue for that hour across eligible orders

        :param days: Weekdays to include in data
        :param start_date: Start date for hourly data
        :param end_date: End date for hourly data
        :return (dict): Dictionary of revenue by hour
        """
        if days is None:
            days = [0, 1, 2, 3, 4, 5, 6]
        if not self.order_data:
            self.order_data = self._get_order_data()
        starting_date = start_date or self.start_date
        ending_date = end_date or self.end_date
        eligible_orders = self.eligible_orders(
            start_date=starting_date, end_date=ending_date
        )
        hourly_revenue = [0] * 24
        for order in eligible_orders:
            order_time = datetime.fromtimestamp(
                order.get("createdTime", 0) / 1000, pytz.timezone("US/Pacific")
            )
            if order_time.weekday() in days:
                hourly_revenue[order_time.hour] += order.get("total", 0) / 100
        return hourly_revenue

    @property
    def ut_figure(self):
        """
        Create bar figure for units per transaction

        :return (dict): UT figure
        """
        figure = {
            "data": [
                {
                    "x": ["This Period"],
                    "y": [
                        self.get_units_per_transaction(
                            start_date=self.start_date, end_date=self.end_date
                        )
                    ],
                    "type": "bar",
                    "name": "This Period",
                },
                {
                    "x": ["Prior Period"],
                    "y": [
                        self.get_units_per_transaction(
                            start_date=self.prior_start, end_date=self.prior_end
                        )
                    ],
                    "type": "bar",
                    "name": "Prior period",
                },
                {
                    "x": ["Monthly"],
                    "y": [
                        self.get_units_per_transaction(
                            start_date=self.month_start, end_date=self.end_date
                        )
                    ],
                    "type": "bar",
                    "name": "Monthly",
                },
            ],
            "layout": {"title": "Units Per Transaction"},
        }
        return figure

    @property
    def at_figure(self):
        """
        Construct bar figure for average transaction

        :return (dict): AT figure
        """
        figure = {
            "data": [
                {
                    "x": ["This Period"],
                    "y": [
                        self.get_average_transaction(
                            start_date=self.start_date, end_date=self.end_date
                        )
                    ],
                    "type": "bar",
                    "name": "This Period",
                },
                {
                    "x": ["Prior Period"],
                    "y": [
                        self.get_average_transaction(
                            start_date=self.prior_start, end_date=self.prior_end
                        )
                    ],
                    "type": "bar",
                    "name": "Prior Period",
                },
                {
                    "x": ["Monthly"],
                    "y": [
                        self.get_average_transaction(
                            start_date=self.month_start, end_date=self.end_date
                        )
                    ],
                    "type": "bar",
                    "name": "Monthly",
                },
            ],
            "layout": {"title": "Average Transaction"},
        }
        return figure

    @property
    def sales_figure(self):
        """
        Construct bar figure for total sales

        :return (dict): Dictionary describing total sales figure
        """
        figure = {
            "data": [
                {
                    "x": ["This Period"],
                    "y": [
                        self.get_sales_totals(
                            start_date=self.start_date, end_date=self.end_date
                        )
                    ],
                    "type": "bar",
                    "name": "This Period",
                },
                {
                    "x": ["Prior Period"],
                    "y": [
                        self.get_sales_totals(
                            start_date=self.prior_start, end_date=self.prior_end
                        )
                    ],
                    "type": "bar",
                    "name": "Prior Period",
                },
            ],
            "layout": {"title": "Sales Totals"},
        }
        return figure

    def hourly_revenue(self, days: list):
        """
        Construct bar chart of hourly revenue for selected days

        :param days: List of days to include
        :return (dict): Dictionary describing hourly revenue figure
        """
        figure = {
            "data": [
                {
                    "x": list(range(24))[8:21],
                    "y": self.get_hourly_revenue(days=days)[8:21],
                    "type": "bar",
                    "name": "hour",
                }
            ]
        }
        return figure

    def update_calendar(self, start_date, end_date):
        """
        Make changes to calendar as needed

        :param start_date: New starting date
        :param end_date: New ending date
        """
        self.start_date = start_date
        self.end_date = end_date
        self.prior_end = self.start_date.shift(days=-1)
        self.prior_start = (
            start_date.shift(weeks=-1)
            if start_date.date() == DEFAULT_START.date()
            and end_date.date() == TODAY.date()
            else self.prior_end.shift(days=(self.start_date - self.end_date).days)
        )
