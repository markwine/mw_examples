# MW Example

Example code featuring a moderately redacted custom shopping cart backend with boilerplate frontend connecting to PayPal, Amazon Pay, USPS, FedEX, and Shipengine, and a dashboard connected to the Clover API for plotting and charting retail data.

Some directories/files have been removed if they were collaborative or sensitive. These are marked foo_emptied.