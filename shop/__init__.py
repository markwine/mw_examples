from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile("config.py", silent=True)

db = SQLAlchemy(app)

from .blueprints.cart.views import cart_blueprint
from .blueprints.static_pages.views import static_blueprint
from .blueprints.products.views_emptied import product_blueprint
from .blueprints.orders.views_emptied import order_blueprint


app.register_blueprint(static_blueprint)
app.register_blueprint(cart_blueprint)
app.register_blueprint(product_blueprint)
app.register_blueprint(order_blueprint)
